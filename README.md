# Implementación de una Red Neuronal Multiclase con Descenso de Gradiente y Entropía Cruzada

Este repositorio contiene una implementación simple de una red neuronal para la clasificación multiclase utilizando Python y NumPy. La red neuronal se entrena utilizando el algoritmo de descenso de gradiente y se evalúa con la pérdida de entropía cruzada. La implementación incluye funciones para inicializar pesos aleatorios, funciones de activación softmax y sigmoidal, así como conversiones de etiquetas categóricas.

## Contenido

- [Implementación de una Red Neuronal Multiclase con Descenso de Gradiente y Entropía Cruzada](#implementación-de-una-red-neuronal-multiclase-con-descenso-de-gradiente-y-entropía-cruzada)
  - [Contenido](#contenido)
  - [Requisitos](#requisitos)
  - [Funciones Principales](#funciones-principales)
  - [Ejemplo de uso](#ejemplo-de-uso)

## Requisitos

- Python 3.x
- NumPy

## Funciones Principales

```generate_random_weights(input_dim, output_dim)``` Genera una matriz de pesos aleatorios para inicializar una red neuronal.``

```softmax(output_vector)```
Calcula la función de activación softmax para convertir puntuaciones en una distribución de probabilidad.

```sigmoid(output_vector)```
Calcula la función de activación sigmoidal.

```crossentropy(y, y_hat)```
Calcula la pérdida de entropía cruzada entre las etiquetas reales y las predichas.

```to_categorical(y)```
Convierte etiquetas de clases enteras a etiquetas categóricas codificadas en one-hot.

```from_categorical(predictions)```
Convierte etiquetas categóricas codificadas en one-hot a etiquetas de clases enteras.

```train_model(X, y, eta=0.1, epochs=2)```
Entrena un modelo usando el algoritmo de descenso de gradiente y entropía cruzada.

```test_model(W, X, y)```
Evalúa un modelo entrenado en un conjunto de datos de prueba.

## Ejemplo de uso

```python
import numpy as np

# Carga tus datos de entrenamiento X_test, y_train aquí...

# Entrena el modelo
W = train_model(X, y, eta=0.1, epochs=100)

# Carga tus datos de prueba X_test, y_test aquí...

# Evalúa el modelo
predictions = test_model(W, X_test, y_test)

```
