import random
import numpy
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix

def generate_random_weights(input_dim, output_dim):
    """
    Genera una matriz de pesos aleatorios para inicializar una red neuronal.
    
    Arguments:
        input_dim (int): Dimensión de entrada.
        output_dim (int): Dimensión de salida.
    
    Returns:
        numpy.ndarray: Matriz de pesos aleatorios.
    """
    W = numpy.empty(shape=(input_dim, output_dim))
    for j in range(0, output_dim, 1):
        for i in range(0, input_dim, 1):
            W[i,j] = random.random()
    return W

def softmax(output_vector):
    """
    Calcula la función de activación softmax para convertir puntuaciones en una distribución de probabilidad.
    
    Arguments:
        vector_salida (numpy.ndarray): Vector de puntuaciones de salida.
    
    Returns:
        numpy.ndarray: Distribución de probabilidad resultante.
    """
    den = 0.0
    for i in range(0, len(output_vector), 1):
        den += numpy.exp(output_vector[i])
    return numpy.exp(output_vector)/den

def sigmoid(output_vector):
    """
    Calcula la función de activación sigmoidal.
    
    Argumentos:
        vector_salida (numpy.ndarray): Vector de puntuaciones de salida.
    
    Returns:
        numpy.ndarray: Resultado después de aplicar la función sigmoidal.
    """
    return 1/(1+numpy.exp(- output_vector))

def crossentropy(y, y_hat):
    """
    Calcula la pérdida de entropía cruzada entre las etiquetas reales y las predichas.
    
    Arguments:
        y (numpy.ndarray): Etiquetas reales codificadas.
        y_hat (numpy.ndarray): Etiquetas predichas.
    
    Returns:
        float: Pérdida de entropía cruzada.
    """
    error = 0
    for i in range(0, len(y), 1):
        error += - y[i] * numpy.log(y_hat[i])
    return error

def to_categorical(y):
    """
    Convierte etiquetas de clases enteras a etiquetas categóricas codificadas en one-hot.
    
    Arguments:
        y (numpy.ndarray): Etiquetas de clases enteras.
    
    Returns:
        numpy.ndarray: Etiquetas categóricas codificadas en one-hot.
    """
    classes = numpy.unique(y)
    new_y = numpy.zeros(shape=(len(y), len(classes)))
    for i in range(0, len(y), 1):
        new_y[i, int(y[i])] = 1
    return new_y

def from_categorical(predictions):
    """
    Convierte etiquetas categóricas codificadas en one-hot a etiquetas de clases enteras.
    
    Arguments:
        predicciones (numpy.ndarray): Etiquetas categóricas codificadas en one-hot.
    
    Returns:
        numpy.ndarray: Etiquetas de clases enteras.
    """
    y_hat = numpy.empty(shape=predictions.shape[0])
    for i in range(0, predictions.shape[0], 1):
        y_hat[i] = numpy.argmax(predictions[i, :])
    return y_hat

def train_model(X, y, eta = 0.1, epochs = 2):
    """
    Entrena un modelo usando el algoritmo de descenso de gradiente y entropía cruzada.
    
    Arguments:
        X (numpy.ndarray): Datos de entrenamiento.
        y (numpy.ndarray): Etiquetas de entrenamiento.
        eta (float): Tasa de aprendizaje.
        epocas (int): Número de épocas de entrenamiento.
    
    Returns:
        numpy.ndarray: Matriz de pesos entrenada.
    """
    nvectors, input_dim = X.shape
    classes = numpy.unique(y)
    output_dim = len(classes)
    y = to_categorical(y)
    W = generate_random_weights(input_dim, output_dim)
    for e in range(1, epochs+1, 1): # epocas
        error = 0.0
        predictions = []
        for i in range(0, nvectors, 1): # vector
            theta = numpy.matmul(W.T, X[i, :]) 
            y_hat = softmax(theta)
            predictions.append(y_hat)
            error += crossentropy(y[i, :], y_hat)
            for k in range(0, input_dim, 1):
                for j in range(0, output_dim, 1):
                    W[k, j] = W[k, j] - eta * (y_hat[j]-y[i, j]) * X[i,k]
        print('Epoca {} - Error: {}'.format(e, error))
    return W
        
def test_model(W, X, y):
    """
    Evalúa un modelo entrenado en un conjunto de datos de prueba.
    
    Arguments:
        W (numpy.ndarray): Matriz de pesos entrenada.
        X (numpy.ndarray): Datos de prueba.
        y (numpy.ndarray): Etiquetas de prueba.
    
    Returns:
        numpy.ndarray: Predicciones del modelo en forma de etiquetas categóricas codificadas.
    """
    nvectors, input_dim = X.shape
    error = 0.0
    predictions = []
    y = to_categorical(y)
    for i in range(0, nvectors, 1): # vector
        theta = numpy.matmul(W.T, X[i, :]) 
        y_hat = softmax(theta)
        predictions.append(y_hat)
        error += crossentropy(y[i, :], y_hat)
    print('Error: {}'.format(error))
    return numpy.vstack(predictions)








